/*
 * Copyright (C) 2022 MURENA SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.lineageos.updater.misc

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.Network
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.preference.PreferenceManager
import org.lineageos.updater.UpdatesCheckReceiver
import org.lineageos.updater.controller.UpdaterController

class ConnectionStateMonitor {
    companion object {
        private var instance: ConnectivityManager.NetworkCallback? = null
    }

    fun getInstance(context: Context): ConnectivityManager.NetworkCallback {
        if (instance == null) {
            instance = networkCallback(context)
        }

        return instance!!
    }

    /**
     * API callbacks to determine which status we currently in
     * we need the below callbacks:
     * - onAvailable: device connected to a network of course
     * - onLost: when the connection completely lost
     */
    private fun networkCallback(context: Context) = object: ConnectivityManager.NetworkCallback() {
        private val tag = "ConnectionStateMonitor"
        private val pref: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        private val updaterController: UpdaterController = UpdaterController.getInstance(context)

        private fun checkForUpdatesOrResume() {
            val downloadId: String = pref.getString(Constants.RESUME_DOWNLOAD_ID, "")!!
            if (downloadId.isNotEmpty()) {
                Handler(Looper.getMainLooper()).postDelayed({
                    updaterController.resumeDownload(
                        downloadId
                    )
                }, 2000L) // 2 seconds
            }
            if (pref.getBoolean(Constants.AUTO_UPDATE_CHECK_FAILED, false)) {
                Handler(Looper.getMainLooper()).postDelayed({
                    val broadcastIntent = Intent()
                    broadcastIntent.setClassName(context, UpdatesCheckReceiver::class.java.name)
                    context.sendBroadcast(broadcastIntent)
                }, 10000L) // 10 seconds
            }

        }

        override fun onAvailable(network: Network) {
            Log.d(tag, "Network available")
            checkForUpdatesOrResume()
        }

        override fun onLost(network: Network) {
            Log.d(tag, "Network not available")
            checkForUpdatesOrResume()
        }
    }
}
