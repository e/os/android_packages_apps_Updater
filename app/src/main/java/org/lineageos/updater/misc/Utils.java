/*
 * Copyright (C) 2017-2023 The LineageOS Project
 * Copyright (C) 2022 ECORP SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.lineageos.updater.misc;

import static android.os.SystemUpdateManager.KEY_STATUS;
import static android.os.SystemUpdateManager.KEY_TITLE;
import static android.os.SystemUpdateManager.STATUS_IDLE;
import static android.os.SystemUpdateManager.STATUS_WAITING_DOWNLOAD;

import android.app.AlarmManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.os.StatFs;
import android.os.SystemProperties;
import android.os.SystemUpdateManager;
import android.os.storage.StorageManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lineageos.updater.R;
import org.lineageos.updater.UpdateImporter;
import org.lineageos.updater.UpdatesDbHelper;
import org.lineageos.updater.controller.UpdaterController;
import org.lineageos.updater.controller.UpdaterService;
import org.lineageos.updater.model.Update;
import org.lineageos.updater.model.UpdateBaseInfo;
import org.lineageos.updater.model.UpdateInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Utils {

    private static final int BATTERY_PLUGGED_ANY = BatteryManager.BATTERY_PLUGGED_AC
            | BatteryManager.BATTERY_PLUGGED_USB
            | BatteryManager.BATTERY_PLUGGED_WIRELESS;

    private static final String TAG = "Utils";
    private static final String CONTENT_URI_PATH = "content://custom.setting.Provider.OTA_SERVER/cte";

    private Utils() {
    }

    public static File getDownloadPath(Context context) {
        return new File(context.getString(R.string.download_path));
    }

    public static File getExportPath(Context context) {
        File dir = new File(context.getExternalFilesDir(null),
                context.getString(R.string.export_path));
        if (!dir.isDirectory()) {
            if (dir.exists() || !dir.mkdirs()) {
                throw new RuntimeException("Could not create directory");
            }
        }
        return dir;
    }

    public static File getCachedUpdateList(Context context) {
        return new File(context.getCacheDir(), "updates.json");
    }

    public static String generateRandomID() {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        return "anon" + uuid;
    }

    // This should really return an UpdateBaseInfo object, but currently this only
    // used to initialize UpdateInfo objects
    private static UpdateInfo parseJsonUpdate(JSONObject object) throws JSONException {
        Update update = new Update();
        update.setTimestamp(object.getLong("datetime"));
        update.setName(object.getString("filename"));
        update.setDownloadId(object.getString("id"));
        update.setType(object.getString("romtype"));
        update.setFileSize(object.getLong("size"));
        update.setDownloadUrl(object.getString("url"));
        update.setVersion(object.getString("version"));
        update.setAndroidVersion(object.getString("android_version"));
        if (object.has("pre_version") && !object.getString("pre_version").isEmpty()) {
          update.setDisplayVersion(object.getString("version") + "-" + object.getString("pre_version"));
        } else {
          update.setDisplayVersion(object.getString("version"));
        }
        return update;
    }

    public static int parseAndroidVersion(String versionString) {
        // Parse android versions such as 8.1.0.
        // Older updates still shows in ota requests.
        Pattern pattern = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)");
        Matcher matcher = pattern.matcher(versionString);
        return matcher.matches() ? Integer.parseInt(Objects.requireNonNull(matcher.group(1)))
                : Float.valueOf(versionString).intValue();
    }

    public static boolean isCompatible(UpdateBaseInfo update) {
        String updateAndroidVersion = update.getAndroidVersion();
        if (!updateAndroidVersion.isEmpty()) {
            final int updateOSVersion = parseAndroidVersion(updateAndroidVersion);
            final int deviceOSVersion = parseAndroidVersion(Build.VERSION.RELEASE);
            if (deviceOSVersion > updateOSVersion) {
                    Log.d(TAG, "Update : Skipping " + update.getName() + " since the installed version "
                            + deviceOSVersion + " is newer than update " + updateOSVersion);
                    return false;
            }
        }

        int[] updateVersionParts = parseSemVer(update.getVersion());
        int updateMajorVersion = updateVersionParts[0];
        int updateMinorVersion = updateVersionParts[1];
        Log.d(TAG, "Update : Major "+updateMajorVersion +" Minor "+ updateMinorVersion );

        int[] deviceVersionParts = parseSemVer(SystemProperties.get(Constants.PROP_BUILD_VERSION));
        int deviceMajorVersion = deviceVersionParts[0];
        int deviceMinorVersion = deviceVersionParts[1];
        Log.d(TAG, "Device : Major "+ deviceMajorVersion +" Minor "+ deviceMinorVersion );

        if (!SystemProperties.getBoolean(Constants.PROP_UPDATER_ALLOW_DOWNGRADING, false) &&
            update.getTimestamp() <= SystemProperties.getLong(Constants.PROP_BUILD_DATE, 0)) {
            Log.d(TAG, update.getName() + " is older than/equal to the current build");
            return false;
        }
        if (!update.getType().equalsIgnoreCase(SystemProperties.get(Constants.PROP_RELEASE_TYPE))) {
            Log.d(TAG, update.getName() + " has type " + update.getType());
            return false;
        }
        if(updateMajorVersion > deviceMajorVersion){
            Log.d(TAG, update.getName() + " is Newer to current Major version");
            return true;
        }
        if(updateMajorVersion < deviceMajorVersion){
            Log.d(TAG, update.getName() + " is Older to current Major version");
            return false;
        }
        if(updateMinorVersion < deviceMinorVersion){
            Log.d(TAG, update.getName() + " is Older to current Minor version");
            return false;
        }

        return true;
    }

    public static int[] parseSemVer(String versionCode) {
        String[] versionParts = versionCode.split(Pattern.quote("."));
        int major = Integer.parseInt(versionParts[0]);
        int minor = Integer.parseInt(versionParts[1]);
        return new int[]{ major, minor };
    }

    public static long availableFreeSpace() {
        StatFs stats = new StatFs(Environment.getDataDirectory().getAbsolutePath());
        return stats.getAvailableBlocksLong() * stats.getBlockSizeLong();
    }

    // https://stackoverflow.com/a/28527441
    public static String getFileSize(long size) {
        if (size <= 0)
            return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static boolean canInstall(UpdateBaseInfo update) {
        return (SystemProperties.getBoolean(Constants.PROP_UPDATER_ALLOW_DOWNGRADING, false) ||
                update.getTimestamp() > SystemProperties.getLong(Constants.PROP_BUILD_DATE, 0));
    }

    public static List<UpdateInfo> parseJson(File file, boolean compatibleOnly)
            throws IOException, JSONException {
        List<UpdateInfo> updates = new ArrayList<>();

        StringBuilder json = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null;) {
                json.append(line);
            }
        }

        JSONObject obj = new JSONObject(json.toString());
        JSONArray updatesList = obj.getJSONArray("response");
        for (int i = 0; i < updatesList.length(); i++) {
            if (updatesList.isNull(i)) {
                continue;
            }
            try {
                boolean isValidated = JsonValidator.validateResponseObject(
                        updatesList.getJSONObject(i));
                if (!isValidated) {
                    Log.d(TAG, "Ignoring incompatible update");
                    continue;
                }
                UpdateInfo update = parseJsonUpdate(updatesList.getJSONObject(i));
                if (!compatibleOnly || isCompatible(update)) {
                    updates.add(update);
                } else {
                    Log.d(TAG, "Ignoring incompatible update " + update.getName());
                }
            } catch (JSONException e) {
                Log.e(TAG, "Could not parse update object, index=" + i, e);
            }
        }

        return updates;
    }

    public static String getServerURL(Context context) {
        String incrementalVersion = SystemProperties.get(Constants.PROP_BUILD_VERSION_INCREMENTAL);
        String device = SystemProperties.get(Constants.PROP_NEXT_DEVICE,
                SystemProperties.get(Constants.PROP_DEVICE));
        String type = SystemProperties.get(Constants.PROP_RELEASE_TYPE).toLowerCase(Locale.ROOT);
        String anonHash = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.OTA_ANON_HASH);

        String serverUrl = SystemProperties.get(Constants.PROP_UPDATER_URI);
        if (retrieveStatus(context) != null && retrieveStatus(context).equals("true")
                && isDevModeOn(context)) {
            serverUrl = context.getString(R.string.ota_staging_server_url);
        }

        if (serverUrl.trim().isEmpty()) {
            serverUrl = context.getString(R.string.updater_server_url);
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean allUpdates = preferences.getBoolean(Constants.PREF_ALL_UPDATES, false);
        if (Utils.isDevModeOn(context)) {
            allUpdates = true;
        }

        if (anonHash != null && !anonHash.isEmpty()) {
            serverUrl += "?ota_anon_hash=" + anonHash + "&strict=" + !allUpdates;
        } else {
            serverUrl += "?strict=" + !allUpdates;
        }

        return serverUrl.replace("{device}", device)
                .replace("{type}", type)
                .replace("{incr}", incrementalVersion);
    }

    /*get the status from database that ota option is on or off*/
    public static String retrieveStatus(Context context) {
        String status = null;
        Cursor cursor = context.getContentResolver().query(Uri.parse(CONTENT_URI_PATH), null, "id=?", new String[]{"1"}, "Status");
        if (cursor.moveToFirst()) {
            do {
                status = cursor.getString(cursor.getColumnIndex("Status"));
            } while (cursor.moveToNext());
        }
        return status;
    }

    public static String getUpgradeBlockedURL(Context context) {
        String device = SystemProperties.get(Constants.PROP_NEXT_DEVICE,
                SystemProperties.get(Constants.PROP_DEVICE));
        return context.getString(R.string.blocked_update_info_url, device);
    }

    public static String getChangelogURL(Context context) {
        String buildVersion = SystemProperties.get(Constants.PROP_BUILD_VERSION);
        return context.getString(R.string.menu_changelog_url, buildVersion);
    }

    public static void triggerUpdate(Context context, String downloadId) {
        final Intent intent = new Intent(context, UpdaterService.class);
        intent.setAction(UpdaterService.ACTION_INSTALL_UPDATE);
        intent.putExtra(UpdaterService.EXTRA_DOWNLOAD_ID, downloadId);
        context.startService(intent);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = context.getSystemService(ConnectivityManager.class);
        Network activeNetwork = cm.getActiveNetwork();
        NetworkCapabilities networkCapabilities = cm.getNetworkCapabilities(activeNetwork);
        if (networkCapabilities != null &&
                networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) &&
                networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)) {
            return networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_USB)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN)
                    || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
        }
        return false;
    }

    public static boolean isNetworkMetered(Context context) {
        ConnectivityManager cm = context.getSystemService(ConnectivityManager.class);
        return cm.isActiveNetworkMetered();
    }

    /**
     * Compares two json formatted updates list files
     *
     * @param newJson new update list
     * @return true if newJson has an update with higher version than the installed system
     */
    public static boolean checkForNewUpdates(File newJson, Context context)
            throws IOException, JSONException {
        List<UpdateInfo> newList = parseJson(newJson, true);
        int[] deviceVersionParts = parseSemVer(SystemProperties.get(Constants.PROP_BUILD_VERSION));
        int deviceMajorVersion = deviceVersionParts[0];
        int deviceMinorVersion = deviceVersionParts[1];
        int deviceMaintenanceVersion = deviceVersionParts.length > 2 ? deviceVersionParts[2] : 0;
        int highestMajorVersion = deviceMajorVersion;
        int highestMinorVersion = deviceMinorVersion;
        int highestMaintenanceVersion = deviceMaintenanceVersion;
	    boolean hasUpdate = false;
        for (UpdateInfo update : newList) {
            if (isCompatible(update)) {
                Log.d(TAG, "New compatible update available");
                int[] updateVersionParts = parseSemVer(update.getVersion());
                int updateMajorVersion = updateVersionParts[0];
                int updateMinorVersion = updateVersionParts[1];
                int updateMaintenanceVersion = updateVersionParts.length > 2
                        ? updateVersionParts[2] : 0;
                if (updateMajorVersion * 10000 + updateMinorVersion * 100 + updateMaintenanceVersion
                        >= highestMajorVersion * 10000 + highestMinorVersion * 100
                        + highestMaintenanceVersion) {
                    highestMajorVersion = updateMajorVersion;
                    highestMinorVersion = updateMinorVersion;
					highestMaintenanceVersion = updateMaintenanceVersion;
				}
				hasUpdate = true;
            }
        }
        String updateVersion = highestMajorVersion + "." + highestMinorVersion +
                (highestMaintenanceVersion > 0 ? "." + highestMaintenanceVersion : "");
        if (hasUpdate) {
           updateSystemUpdaterService(context, STATUS_WAITING_DOWNLOAD, updateVersion);
           return true;
        } else {
           updateSystemUpdaterService(context, STATUS_IDLE, updateVersion);
           return false;
        }
    }

    /**
     * Get the offset to the compressed data of a file inside the given zip
     *
     * @param zipFile input zip file
     * @param entryPath full path of the entry
     * @return the offset of the compressed, or -1 if not found
     * @throws IllegalArgumentException if the given entry is not found
     */
    public static long getZipEntryOffset(ZipFile zipFile, String entryPath) {
        // Each entry has an header of (30 + n + m) bytes
        // 'n' is the length of the file name
        // 'm' is the length of the extra field
        final int FIXED_HEADER_SIZE = 30;
        Enumeration<? extends ZipEntry> zipEntries = zipFile.entries();
        long offset = 0;
        while (zipEntries.hasMoreElements()) {
            ZipEntry entry = zipEntries.nextElement();
            int n = entry.getName().length();
            int m = entry.getExtra() == null ? 0 : entry.getExtra().length;
            int headerSize = FIXED_HEADER_SIZE + n + m;
            offset += headerSize;
            if (entry.getName().equals(entryPath)) {
                return offset;
            }
            offset += entry.getCompressedSize();
        }
        Log.e(TAG, "Entry " + entryPath + " not found");
        throw new IllegalArgumentException("The given entry was not found");
    }

    public static void removeUncryptFiles(File downloadPath) {
        File[] uncryptFiles = downloadPath.listFiles(
                (dir, name) -> name.endsWith(Constants.UNCRYPT_FILE_EXT));
        if (uncryptFiles == null) {
            return;
        }
        for (File file : uncryptFiles) {
            //noinspection ResultOfMethodCallIgnored
            file.delete();
        }
    }

    public static void removeLocalUpdate(Context context) {
        File downloadPath = getDownloadPath(context);
        File localUpdate = new File(downloadPath, UpdateImporter.FILE_NAME);
        if (localUpdate.exists()) {
            Log.d(TAG, "Deleting local update: " + localUpdate.delete());
        }
    }

    /**
     * Cleanup the download directory, which is assumed to be a privileged location
     * the user can't access and that might have stale files. This can happen if
     * the data of the application are wiped.
     *
     */
    public static void cleanupDownloadsDir(Context context) {
        File downloadPath = getDownloadPath(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        removeUncryptFiles(downloadPath);

        long buildTimestamp = SystemProperties.getLong(Constants.PROP_BUILD_DATE, 0);
        long prevTimestamp = preferences.getLong(Constants.PREF_INSTALL_OLD_TIMESTAMP, 0);
        String lastUpdatePath = preferences.getString(Constants.PREF_INSTALL_PACKAGE_PATH, null);
        boolean reinstalling = preferences.getBoolean(Constants.PREF_INSTALL_AGAIN, false);
        boolean deleteUpdates = preferences.getBoolean(Constants.PREF_AUTO_DELETE_UPDATES, true);
        if ((buildTimestamp != prevTimestamp || reinstalling) && deleteUpdates &&
                lastUpdatePath != null) {
            File lastUpdate = new File(lastUpdatePath);
            if (lastUpdate.exists()) {
                //noinspection ResultOfMethodCallIgnored
                lastUpdate.delete();
                // Remove the pref not to delete the file if re-downloaded
                preferences.edit().remove(Constants.PREF_INSTALL_PACKAGE_PATH).apply();
            }
        }

        final String DOWNLOADS_CLEANUP_DONE = "cleanup_done";
        if (preferences.getBoolean(DOWNLOADS_CLEANUP_DONE, false)) {
            return;
        }

        Log.d(TAG, "Cleaning " + downloadPath);
        if (!downloadPath.isDirectory()) {
            return;
        }
        File[] files = downloadPath.listFiles();
        if (files == null) {
            return;
        }

        // Ideally the database is empty when we get here
        UpdatesDbHelper dbHelper = new UpdatesDbHelper(context);
        List<String> knownPaths = new ArrayList<>();
        for (UpdateInfo update : dbHelper.getUpdates()) {
            knownPaths.add(update.getFile().getAbsolutePath());
        }
        for (File file : files) {
            if (!knownPaths.contains(file.getAbsolutePath())) {
                Log.d(TAG, "Deleting " + file.getAbsolutePath());
                //noinspection ResultOfMethodCallIgnored
                file.delete();
            }
        }

        preferences.edit().putBoolean(DOWNLOADS_CLEANUP_DONE, true).apply();
    }

    public static void removeInstalledUpdate(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String downloadId = pref.getString(Constants.PREF_NEEDS_DELETE_ID, null);
        if (downloadId != null) {
            UpdaterController controller = UpdaterController.getInstance(context);
            if (controller != null) {
                controller.deleteUpdate(downloadId);
            }
            pref.edit().remove(Constants.PREF_NEEDS_DELETE_ID).apply();
        }
    }

    public static File appendSequentialNumber(final File file) {
        String name;
        String extension;
        int extensionPosition = file.getName().lastIndexOf(".");
        if (extensionPosition > 0) {
            name = file.getName().substring(0, extensionPosition);
            extension = file.getName().substring(extensionPosition);
        } else {
            name = file.getName();
            extension = "";
        }
        final File parent = file.getParentFile();
        for (int i = 1; i < Integer.MAX_VALUE; i++) {
            File newFile = new File(parent, name + "-" + i + extension);
            if (!newFile.exists()) {
                return newFile;
            }
        }
        throw new IllegalStateException();
    }

    public static boolean isABDevice() {
        return SystemProperties.getBoolean(Constants.PROP_AB_DEVICE, false);
    }

    public static boolean isABUpdate(ZipFile zipFile) {
        return zipFile.getEntry(Constants.AB_PAYLOAD_BIN_PATH) != null &&
                zipFile.getEntry(Constants.AB_PAYLOAD_PROPERTIES_PATH) != null;
    }

    public static boolean isABUpdate(File file) throws IOException {
        ZipFile zipFile = new ZipFile(file);
        boolean isAB = isABUpdate(zipFile);
        zipFile.close();
        return isAB;
    }

    public static boolean defaultABPerfMode(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean oldPrefValue = prefs.getBoolean(Constants.PREF_AB_PERF_MODE_OLD, false);
        return (context.getResources().getBoolean(R.bool.config_prioritizeUpdateProcess)
                || oldPrefValue);
    }

    public static boolean isDevModeOn(Context context) {
        return Settings.Secure.getInt(context.getContentResolver(),
                Settings.Global.DEVELOPMENT_SETTINGS_ENABLED , 0) == 1;
    }

    public static boolean hasTouchscreen(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TOUCHSCREEN);
    }

    public static void addToClipboard(Context context, String label, String text,
                                      String toastMessage) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(
                Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(label, text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();
    }

    public static boolean isEncrypted(Context context, File file) {
        StorageManager sm = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        return sm.isEncrypted(file);
    }

    public static int getUpdateCheckSetting(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(Constants.PREF_AUTO_UPDATES_CHECK_INTERVAL,
                Constants.AUTO_UPDATES_CHECK_INTERVAL_DAILY);
    }

    public static boolean isUpdateCheckEnabled(Context context) {
        return getUpdateCheckSetting(context) != Constants.AUTO_UPDATES_CHECK_INTERVAL_NEVER;
    }

    public static long getUpdateCheckInterval(Context context) {
        switch (Utils.getUpdateCheckSetting(context)) {
            case Constants.AUTO_UPDATES_CHECK_INTERVAL_5_MINUTES:
                return AlarmManager.INTERVAL_FIFTEEN_MINUTES / 3;
            case Constants.AUTO_UPDATES_CHECK_INTERVAL_10_MINUTES:
                return AlarmManager.INTERVAL_HALF_HOUR / 3;
            case Constants.AUTO_UPDATES_CHECK_INTERVAL_30_MINUTES:
                return AlarmManager.INTERVAL_HALF_HOUR;
            case Constants.AUTO_UPDATES_CHECK_INTERVAL_DAILY:
                return AlarmManager.INTERVAL_DAY;
            case Constants.AUTO_UPDATES_CHECK_INTERVAL_WEEKLY:
            default:
                return AlarmManager.INTERVAL_DAY * 7;
            case Constants.AUTO_UPDATES_CHECK_INTERVAL_MONTHLY:
                return AlarmManager.INTERVAL_DAY * 30;
        }
    }

    public static boolean isRecoveryUpdateExecPresent() {
        return new File(Constants.UPDATE_RECOVERY_EXEC).exists();
    }

    public static boolean isBatteryLevelOk(Context context) {
        Intent intent = context.registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        if (!intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, false)) {
            return true;
        }
        int percent = Math.round(100.f * intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 100) /
                intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100));
        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0);
        int required = (plugged & BATTERY_PLUGGED_ANY) != 0 ?
                context.getResources().getInteger(R.integer.battery_ok_percentage_charging) :
                context.getResources().getInteger(R.integer.battery_ok_percentage_discharging);
        return percent >= required;
    }

    public static void updateSystemUpdaterService(Context context, int status, String version) {
        final SystemUpdateManager updateManager = context.getSystemService(SystemUpdateManager.class);

        final Bundle oldInfo = updateManager.retrieveSystemUpdateInfo();
        final int oldStatus = oldInfo.getInt(SystemUpdateManager.KEY_STATUS);

        if (status != oldStatus) {
            PersistableBundle infoBundle = new PersistableBundle();
            infoBundle.putInt(KEY_STATUS, status);
            infoBundle.putString(KEY_TITLE, version);
            updateManager.updateSystemUpdateInfo(infoBundle);
        }
    }

    public static boolean isInteger(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
