/*
 * Copyright (C) 2017-2022 The LineageOS Project
 * Copyright (C) 2020-2022 SHIFT GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.lineageos.updater;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.util.Log;

import org.lineageos.updater.controller.UpdaterController;
import org.lineageos.updater.misc.StringGenerator;
import org.lineageos.updater.misc.Utils;
import org.lineageos.updater.model.Update;
import org.lineageos.updater.model.UpdateStatus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.util.Enumeration;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class UpdateImporter {
    private static final int REQUEST_PICK = 9061;
    private static final String TAG = "UpdateImporter";
    private static final String MIME_ZIP = "application/zip";
    public static final String FILE_NAME = "localUpdate.zip";
    private static final String METADATA_PATH = "META-INF/com/android/metadata";
    private static final String METADATA_TIMESTAMP_KEY = "post-timestamp=";
    private static final String METADATA_ANDROID_SDK_KEY = "post-sdk-level=";

    private final Activity activity;
    private final Callbacks callbacks;

    private Thread workingThread;
    private String filename;

    public UpdateImporter(Activity activity, Callbacks callbacks) {
        this.activity = activity;
        this.callbacks = callbacks;
    }

    public void stopImport() {
        if (workingThread != null && workingThread.isAlive()) {
            workingThread.interrupt();
            workingThread = null;
        }
    }

    public void openImportPicker() {
        final Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT)
                .addCategory(Intent.CATEGORY_OPENABLE)
                .setType(MIME_ZIP);
        activity.startActivityForResult(intent, REQUEST_PICK);
    }

    public boolean onResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK || requestCode != REQUEST_PICK) {
            return false;
        }

        return onPicked(data.getData());
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private boolean onPicked(Uri uri) {
        callbacks.onImportStarted();

        workingThread = new Thread(() -> {
            File importedFile = null;
            try {
                importedFile = importFile(uri);
                filename = getFileNameFromUri(uri);
                verifyPackage(importedFile);

                final Update update = buildLocalUpdate(importedFile);
                addUpdate(update);
                activity.runOnUiThread(() -> callbacks.onImportCompleted(update));
            } catch (Exception e) {
                Log.e(TAG, "Failed to import update package", e);
                // Do not store invalid update
                if (importedFile != null) {
                    importedFile.delete();
                }

                activity.runOnUiThread(() -> callbacks.onImportCompleted(null));
            }
        });
        workingThread.start();
        return true;
    }

    public String getFileNameFromUri(Uri uri) {
        String fileName = null;
        if (Objects.equals(uri.getScheme(), "content")) {
            ContentResolver contentResolver = activity.getContentResolver();
            Cursor cursor = contentResolver.query(uri, null, null, null, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        int displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        if (displayNameIndex != -1) {
                            fileName = cursor.getString(displayNameIndex);
                        }
                    }
                } finally {
                    cursor.close();
                }
            }
        } else if (Objects.equals(uri.getScheme(), "file")) {
            fileName = uri.getLastPathSegment();
        }
        return fileName;
    }

    @SuppressLint("SetWorldReadable")
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private File importFile(Uri uri) throws IOException {
        final ParcelFileDescriptor parcelDescriptor = activity.getContentResolver()
                .openFileDescriptor(uri, "r");
        if (parcelDescriptor == null) {
            throw new IOException("Failed to obtain fileDescriptor");
        }

        final FileInputStream iStream = new FileInputStream(parcelDescriptor
                .getFileDescriptor());
        final File downloadDir = Utils.getDownloadPath(activity);
        final File outFile = new File(downloadDir, FILE_NAME);
        if (outFile.exists()) {
            outFile.delete();
        }
        final FileOutputStream oStream = new FileOutputStream(outFile);

        int read;
        final byte[] buffer = new byte[4096];
        while ((read = iStream.read(buffer)) > 0) {
            oStream.write(buffer, 0, read);
        }
        oStream.flush();
        oStream.close();
        iStream.close();
        parcelDescriptor.close();

        outFile.setReadable(true, false);

        return outFile;
    }

    private Update buildLocalUpdate(File file) {
        final long timeStamp = getTimeStamp(file);
        final String buildDate = StringGenerator.getDateLocalizedUTC(
                activity, DateFormat.MEDIUM, timeStamp);
        String name = activity.getString(R.string.local_update_name);
        final String androidVersion = getAndroidVersion(file);
        final Update update = new Update();

        if (filename != null) {
            Pattern pattern = Pattern.compile("-(\\d+\\.\\d+(\\.\\d+)?)");
            Matcher matcher = pattern.matcher(filename);
            if (matcher.find()) {
                name = matcher.group(1);
            }
        }

        update.setAvailableOnline(false);
        update.setName(name);
        update.setFile(file);
        update.setFileSize(file.length());
        update.setDownloadId(Update.LOCAL_ID);
        update.setTimestamp(timeStamp);
        update.setStatus(UpdateStatus.VERIFIED);
        update.setPersistentStatus(UpdateStatus.Persistent.VERIFIED);
        update.setVersion(String.format("%s (%s)", name, buildDate));
        update.setAndroidVersion(androidVersion);
        update.setDisplayVersion(name);
        return update;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void verifyPackage(File file) throws Exception {
        try {
            android.os.RecoverySystem.verifyPackage(file, null, null);
        } catch (Exception e) {
            if (file.exists()) {
                file.delete();
                throw new Exception("Verification failed, file has been deleted");
            } else {
                throw e;
            }
        }
    }

    private void addUpdate(Update update) {
        UpdaterController controller = UpdaterController.getInstance(activity);
        controller.addUpdate(update, false);
    }

    private long getTimeStamp(File file) {
        String timeStamp = getFromMetadata(file, METADATA_TIMESTAMP_KEY);
        if (timeStamp != null) {
            return Long.parseLong(timeStamp);
        }
        return System.currentTimeMillis();
    }

    private String getAndroidVersion(File file) {
        String sdkInt = getFromMetadata(file, METADATA_ANDROID_SDK_KEY);
        if (sdkInt != null) {
            return getAndroidVersionFromSDKInt(Integer.parseInt(sdkInt));
        }
        return String.valueOf(Build.VERSION.RELEASE);
    }

    public static String getAndroidVersionFromSDKInt(int sdkInt) {
        switch (sdkInt) {
            case Build.VERSION_CODES.Q:
                return "10";
            case Build.VERSION_CODES.R:
                return "11";
            case Build.VERSION_CODES.S:
            case Build.VERSION_CODES.S_V2:
                return "12";
            case Build.VERSION_CODES.TIRAMISU:
                return "13";
            case 34: // Build.VERSION_CODES.UPSIDE_DOWN_CAKE
                return "14";
            default:
                return Build.VERSION.RELEASE;
        }
    }

    private String getFromMetadata(File file, String key) {
        try {
            final String metadataContent = readZippedFile(file, METADATA_PATH);
            final String[] lines = metadataContent.split("\n");
            for (String line : lines) {
                if (!line.startsWith(key)) {
                    continue;
                }

                return line.replace(key, "");
            }
        } catch (IOException e) {
            Log.e(TAG, "Failed to read date from local update zip package", e);
        } catch (NumberFormatException e) {
            Log.e(TAG, "Failed to parse from zip metadata file", e);
        }

        Log.e(TAG, "Couldn't find metadata in zip file, falling back to 0");
        return null;
    }

    private String readZippedFile(File file, String path) throws IOException {
        final StringBuilder sb = new StringBuilder();
        InputStream iStream = null;

        try (final ZipFile zip = new ZipFile(file)) {
            final Enumeration<? extends ZipEntry> iterator = zip.entries();
            while (iterator.hasMoreElements()) {
                final ZipEntry entry = iterator.nextElement();
                if (!METADATA_PATH.equals(entry.getName())) {
                    continue;
                }

                iStream = zip.getInputStream(entry);
                break;
            }

            if (iStream == null) {
                throw new FileNotFoundException("Couldn't find " + path + " in " + file.getName());
            }

            final byte[] buffer = new byte[1024];
            int read;
            while ((read = iStream.read(buffer)) > 0) {
                sb.append(new String(buffer, 0, read, StandardCharsets.UTF_8));
            }
        } catch (IOException e) {
            Log.e(TAG, "Failed to read file from zip package", e);
            throw e;
        } finally {
            if (iStream != null) {
                iStream.close();
            }
        }

        return sb.toString();
    }

    public interface Callbacks {
        void onImportStarted();

        void onImportCompleted(Update update);
    }
}
